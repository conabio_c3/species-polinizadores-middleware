
colecciones = {}

// colecciones.resoluciones_regulares = [8, 16, 32, 64, "state", "mun", "ageb", "cue"];
colecciones.resoluciones_regulares = [8];
// colecciones.resoluciones_irregulares = ["state", "mun", "ageb", "cue"];

// colecciones.regiones = [1 ,2 ,3 ,4, 5];
colecciones.regiones = [2];

colecciones.spobj = [
	{
		nivel: 8,
		taxon_rank: "species",
		title: "test_group",
		value: "Lynx rufus"
	},
	{
		nivel: 8,
		taxon_rank: "species",
		title: "test_group",
		value: "Panthera onca"
	},
	// {
	// 	nivel: 8,
	// 	taxon_rank: "species",
	// 	title: "test_group",
	// 	value: "Triatoma barbieri"
	// },
	// {
	// 	nivel: 8,
	// 	taxon_rank: "species",
	// 	title: "test_group",
	// 	value: "Aedes aegypti"
	// },
	// {
	// 	nivel: 8,
	// 	taxon_rank: "species",
	// 	title: "test_group",
	// 	value: "Lutzomyia cruciata"
	// },
	// {
	// 	nivel: 8,
	// 	taxon_rank: "species",
	// 	title: "test_group",
	// 	value: "Oryctolagus cuniculus"
	// },
	// {
	// 	nivel: 8,
	// 	taxon_rank: "species",
	// 	title: "test_group",
	// 	value: "Quiscalus mexicanus"
	// },
	// {
	// 	nivel: 8,
	// 	taxon_rank: "species",
	// 	title: "test_group",
	// 	value: "Passerculus sandwichensis"
	// },
	// {
	// 	nivel: 8,
	// 	taxon_rank: "species",
	// 	title: "test_group",
	// 	value: "Sitta pusilla"
	// }
	// {
	// 	nivel: 8,
	// 	taxon_rank: "species",
	// 	title: "test_group",
	// 	value: "Corvus ossifragus"
	// },
	// {
	// 	nivel: 8,
	// 	taxon_rank: "species",
	// 	title: "test_group",
	// 	value: "Zonotrichia querula"
	// }	
];


colecciones.covariables = [
	{
		biotic: true,
		group_item: 1,
		merge_vars: [{
			level: "species",
			rank: "class",
			type: 0,
			value: "Mammalia"
		}],
		name: "mamiferos"
	}
	// {
	// 	biotic: true,
	// 	group_item: 1,
	// 	merge_vars: [{
	// 		level: "species",
	// 		rank: "class",
	// 		type: 0,
	// 		value: "Aves"
	// 	}],
	// 	name: "aves"
	// },
	// {
	// 	biotic:	false,
	// 	group_item:	1,
	// 	merge_vars: [{
	// 		level:"bid",
	// 		rank:"type",
	// 		type:1,
	// 		value:1
	// 	}],
	// 	name: "Worldclim"
	// }
];

// filtros_sptarget = [["false", "false", "false"], ["false", "false", "true"], ["true", "true", "true"], ["true", "true", "false"], ["false", "true", "false"], ["false", "true", "true"], ["true", "false", "true"], ["true", "false", "false"]]
// ba_var = [["true", "false"],["false", "true"],["true", "true"]]
// apr_prob = [["false", "false"], ["true", "false"],["false", "true"], ["true", "true"]]

module.exports = colecciones

