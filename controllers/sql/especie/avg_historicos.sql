select 
avg(tiempo_proceso_solicitud_grupotx) as avg_proceso,  
AVG(fecha_display_solicitud_grupotx - fecha_inicio_solicitud_grupotx) as avg_display
from historicos_seleccion_gruposobj
where 
region = $<region:raw>
and resolucion = '$<grid_res:raw>'
-- and rango_fecha = $<rango_fecha:raw>
and reg_sfecha = $<sfecha:raw>
and reg_fosil = $<sfosil:raw>
-- and fecha_inicio = $<liminf:raw>
-- and fecha_fin = $<limsup:raw>
-- and num_celdas_spo = 309
-- and num_poligonos_totales = 9839
and es_solicitudmalla = 1
and iscarga_datos = $<iscarga_datos:raw>
and tipo_procedencia = '$<tipo_procedencia:raw>'
and fecha_inicio_solicitud_grupotx is not null
and fecha_display_solicitud_grupotx is not null