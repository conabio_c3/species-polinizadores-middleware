UPDATE historicos_analisis
SET 
tiempo_display_analisis = $<final_time:raw>, 
fecha_display = '$<timestamp_display:raw>'::timestamp
WHERE id_analisis = $<idunico:raw>
RETURNING id_analisis;