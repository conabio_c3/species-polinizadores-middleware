
function importTest(name, path) {
    describe(name, function () {
        require(path);
    });
}

beforeEach(function () {
   console.log('\x1b[1m', "\n>>>>>>>>>>>> Inicio prueba\n\n");
});

afterEach(function () {
    console.log('\x1b[1m', "\n<<<<<<<<<<<< Termina prueba\n\n\n");
});

// importTest("\n", './nicho/nicho_test');
// importTest("\n", './redes/redes_test');
importTest("\n", './historicos/historicos_test');