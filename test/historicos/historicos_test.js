var supertest = require("supertest");
var should = require("should");
var expect = require('chai').expect;
var moment = require('moment')
var verb_utils = require('../../controllers/verb_utils')
var colecciones = require('../colecciones')

var chai = require("chai");
// chai.should();
chai.use(require('chai-things'));

var config = require('../../config')


var server;
var ambiente = config.test.ambiente;
var base = config.test.db;
// var nivel = config.test.profundidad;

// TODO: sets para pruebas con diferentes especies
var sets = [{}];

console.info("************************************************\n")
console.info("\tINICIO DE INSERCIÓN DE HISTORICOS SOLICITUD Y ANALISIS\n")
console.info("\tAmbiente: " + ambiente  + "\n")
console.info("\tBase: " + base  + "\n")
// console.info("\tProfundidad: " + nivel  + "\n")
console.info("************************************************\n")

beforeEach(function () {
	delete require.cache[require.resolve('../../server')]
	server = require('../../server')
})

afterEach(function (done) {
	server.close(done)
})

// configuracion de variables para produnfidad de pruebas
var resoluciones = []
var regiones = []
var colecciones_spobj = []
var covariables = []
var _timestamp_request;
// var filtros_sptarget = [] // fecha, rango y fosil
// var ba_var = [] // bioticos y abioticos
// var apr_prob = [] // apriori y probaildiad


resoluciones = colecciones.resoluciones_regulares;
// resoluciones = colecciones.resoluciones_irregulares;
regiones = colecciones.regiones;
colecciones_spobj = colecciones.spobj;
covariables = colecciones.covariables;


describe("Prueba: Petición de seleccion de grupo objetivo con carga de malla para registro de históricos",function(){
	
	this.timeout(1000 * 60 * 30);

	resoluciones.forEach(cell_res => { // prueba de todas las resoluciones regulares

		regiones.forEach(footprint_region => { // Se pueden agregar mas regiones en archivo de colecciones

			colecciones_spobj.forEach(target_taxons => { // Se pueden agregar mas grupos en archivo de colecciones

				covariables.forEach(covariables_col => { // Se pueden agregar mas grupos en archivo de colecciones


					_timestamp_request =  verb_utils.obtieneTimeStamp();
			    	console.log("timestamp_request: " + _timestamp_request);
			    
			    	var isgrid_request = 1;
			    	console.log("isgrid_request: " + isgrid_request);

					it("Resultado: Petición de la malla de " + cell_res + "km en la region: " + footprint_region + ", obtención de grupo objetivo y análisis de nicho - OK", function(done){
						
						supertest(server).post("/niche/especie/getGridGeoJson")
						.send({
							grid_res : ""+cell_res, 
							footprint_region:""+footprint_region,
							timestamp_solicitud: _timestamp_request,
		                	isgrid_request: isgrid_request,
		                	tipo_procedencia: 'API PRUEBA',
							entorno_fb: config.test.ambiente
						})
						.expect("Content-type",/json/)
						.expect(200)
						.end(function(err, response){

							var startimedisplaytinitgrid = new Date().getTime() / 1000;
							console.log("startimedisplaytinitgrid: " + startimedisplaytinitgrid)

							// TODO: Como agregar proceso de despliegue de mallas

							var milliseconds = new Date().getTime();

							var id_registro = response.body.id_registro
							console.log("id_registro: " + id_registro)					

							var tiempo_inicio_solicitud_grupotx = response.body.tiempo_inicio_solicitud_grupotx
							console.log("tiempo_inicio_solicitud_grupotx: " + tiempo_inicio_solicitud_grupotx)					

							var iscarga_datos = 0
							console.log("iscarga_datos: " + iscarga_datos)	

							var lim_inf = 1500;
							var lim_sup = parseInt(moment().format('YYYY'));

							
							var temp_time = new Date().getTime() / 1000
							var endtimedisplaytinitgrid = temp_time - startimedisplaytinitgrid;
	    					console.log("endtimedisplaytinitgrid: " + endtimedisplaytinitgrid)
							


							supertest(server).post("/niche/especie/getGridSpeciesTaxon")
							.send({
								idtime: milliseconds,
								id_registro: id_registro,
								tiempo_inicio_solicitud_grupotx: tiempo_inicio_solicitud_grupotx,
								endtimedisplaytinitgrid: endtimedisplaytinitgrid,
								target_taxons: [target_taxons],
								name: "k",
								sfecha: "false", 
								sfosil: "false",
								liminf: lim_inf,
								limsup: lim_sup,
								idtime: "1549052020331",
								grid_res: cell_res,
								region: footprint_region,
								iscarga_datos: 0
							})
							.expect("Content-type",/json/)
							.expect(200)
							.end(function(err, response){

								// console.log(response.body)					

								var id_registro = response.body.id_registro
								console.log("id_registro: " + id_registro)					

								var tiempo_proceso_solicitud_grupotx = response.body.tiempo_proceso_solicitud_grupotx
								console.log("tiempo_proceso_solicitud_grupotx: " + tiempo_proceso_solicitud_grupotx)					

								var temp_time = new Date().getTime() / 1000
				                var endtimedisplaytinitgrid = tiempo_proceso_solicitud_grupotx + (temp_time - startimedisplaytinitgrid);
				                console.log("endtimedisplaytinitgrid: " + endtimedisplaytinitgrid)

				                _timestamp_request =  verb_utils.obtieneTimeStamp();
								console.log("timestamp_request: " + _timestamp_request);

								var occ_target = response.body.data.length;
								console.log("***** occ_target: " + occ_target);



								supertest(server).post("/niche/updateTimeDislayTaxonGroup")
								.send({
									id_registro: id_registro,
				                    timestamp_request: _timestamp_request,
				                    endtimedisplaytinitgrid: endtimedisplaytinitgrid
								})
								.expect("Content-type",/json/)
								.expect(200)
								.end(function(err, response){

									// INICIAN PRUBEAS DE ANALISIS DE NICHO
									var id_timestamp = new Date().getTime()
									var timestamp = verb_utils.obtieneTimeStamp();
        							console.log("timestamp: " + timestamp);
        							console.log("config.test.ambiente: " + config.test.ambiente);

									supertest(server).post("/niche/countsTaxonsGroup")
									.send({
										
										target_taxons: [target_taxons],
										target_points: [],
										idtime: id_timestamp,
										apriori: false,
										mapa_prob: false,
										min_cells: 5,
										fosil: true,
										lim_inf:1500,
										lim_sup:parseInt(moment().format('YYYY')),
										idtabla: "",
										grid_resolution: cell_res,
										region: footprint_region,
										get_grid_species: false,
										date: true,
										with_data_score_cell: true,
										with_data_freq: true,
										with_data_freq_cell: true,
										with_data_score_decil: true,
										excluded_cells: [],
										target_name: "targetGroup",
										iterations: 1,
										cell_target: occ_target,
							            cell_region: 0,
							            id_timestamp: id_timestamp,
										covariables: [covariables_col],
										decil_selected: [10],
										timestamp_request: timestamp,
										tipo_procedencia: 'API PRUEBA',
							            entorno_fb: config.test.ambiente
										
									})
									.expect("Content-type",/json/)
									.expect(200)
									.end(function(err, response){

										// console.log(response.body)					

										var id_unico = response.body.id_unico
										console.log("id_unico: " + id_unico);

										var id_global = response.body.id_global
										console.log("id_global: " + id_global);

										var final_time = response.body.final_time
										console.log("final_time: " + final_time);

										var timestamp_display = verb_utils.obtieneTimeStamp();
        								console.log("timestamp_display: " + timestamp_display);

        								var end_time = new Date().getTime() / 1000;
        								console.log("start_time: " + end_time);
        								console.log("end_time: " + end_time);


										supertest(server).post("/niche/updateTimeAnalysis")
										.send({
											
											idunico: id_unico,
							                idglobal: id_global,
							                tiempoproceso: final_time,
							                start_time: end_time,
							                end_time: end_time,
							                timestamp_display: timestamp_display
											
										})
										.expect("Content-type",/json/)
										.expect(200)
										.end(function(err, response){

											response.statusCode.should.equal(200);
											done();

										})


									})


									// response.statusCode.should.equal(200)
									// done();

								})


								// response.statusCode.should.equal(200)
								// expect(response.body.data).all.have.property("gridid")
								// expect(response.body.data).all.have.property("occ")
								// done();

							})

							
							// response.statusCode.should.equal(200)
							// expect(response.body.features.properties).all.have.property('gridid');
							// response.res.text.includes('FeatureCollection').should.equal(true)
							// response.res.text.includes('features').should.equal(true)
							// done();
						})


					});


				});


			});


		});

	});
});


// filtros_sptarget.forEach(pair => {

// 	describe("Prueba: Obtención de celdas del grupo objetivo, variando fecha, rango y fosil", function(done){

// 		this.timeout(1000 * 60 * 2); // 3 minutos maximo
		
// 		var lim_inf = 1500
// 		var lim_sup = parseInt(moment().format('YYYY'));

// 		if(pair[1] === "false"){
// 			lim_inf = 2000
// 			lim_sup = 2020
// 		}

// 		var target_taxons = [{
// 			nivel: 8,
// 			taxon_rank: "species",
// 			title: "test_group",
// 			value: "Lynx rufus"
// 		}]

// 		describe("Prueba: fecha | rango | fosil => " + pair,function(){

// 			it("Resultado: fecha | rango | fosil => " + pair + " - OK", function(done){

// 				supertest(server).post("/niche/especie/getGridSpeciesTaxon")
// 				.send({
// 					target_taxons: target_taxons,
// 					name: "k",
// 					sfecha: pair[0], 
// 					sfosil: pair[2],
// 					liminf: lim_inf,
// 					limsup: lim_sup,
// 					idtime: "1549052020331",
// 					grid_res: 16,
// 					region: 1
// 				})
// 				.expect("Content-type",/json/)
// 				.expect(200)
// 				.end(function(err, response){
// 					response.statusCode.should.equal(200)
// 					expect(response.body.data).all.have.property("gridid")
// 					expect(response.body.data).all.have.property("occ")


// 					done();
// 				})

// 			});


// 		})

// 	});

// });



// filtros_sptarget.forEach(pair => {

// 	describe("Prueba: Obtención de celdas del grupo objetivo por carga de archivo, variando fecha, rango y fosil", function(done){

// 		this.timeout(1000 * 60 * 2); // 3 minutos maximo
		
// 		var lim_inf = 1500
// 		var lim_sup = parseInt(moment().format('YYYY'));
		
// 		if(pair[1] === "false"){
// 			lim_inf = 2000
// 			lim_sup = 2020
// 		}

// 		var target_points = [{
// 			anio: 9999,
// 			fosil: "NO",
// 			latitud: 16.799892,
// 			longitud: -93.266542
// 		},
// 		{
// 			anio: 9999,
// 			fosil: "NO",
// 			latitud: 16.93772,
// 			longitud: -93.30668
// 		},
// 		{
// 			anio: 9999,
// 			fosil: "NO",
// 			latitud: 15.530939,
// 			longitud: -105.082803
// 		}]

// 		describe("Prueba: Archivo, fecha | rango | fosil => " + pair,function(){

// 			it("Resultado: Archivo, fecha | rango | fosil => " + pair + " - OK", function(done){

// 				supertest(server).post("/niche/especie/getGridGivenPoints")
// 				.send({
// 					target_points: target_points,
// 					date: pair[0], 
// 					fosil: pair[2],
// 					lim_inf: lim_inf,
// 					lim_sup: lim_sup,
// 					idtime: "1549052020331",
// 					grid_resolution: 16,
// 					region: 1
// 				})
// 				.expect("Content-type",/json/)
// 				.expect(200)
// 				.end(function(err, response){
// 					response.statusCode.should.equal(200)
// 					expect(response.body.data).all.have.property("gridid")
// 					expect(response.body.data).all.have.property("occ")
// 					done();
// 				})

// 			});


// 		})

// 	});

// });
