var supertest = require("supertest");
var should = require("should");
var expect = require('chai').expect;
var moment = require('moment')

var chai = require("chai");
// chai.should();
chai.use(require('chai-things'));

var config = require('../../config')


var server;
var ambiente = config.test.ambiente;
var base = config.test.db;
var nivel = config.test.profundidad;

// TODO: sets para pruebas con diferentes especies
var sets = [{}];

console.info("************************************************\n")
console.info("\tINICIO DE PRUEBAS EN REDES\n")
console.info("\tAmbiente: " + ambiente  + "\n")
console.info("\tBase: " + base  + "\n")
console.info("\tProfundidad: " + nivel  + "\n")
console.info("************************************************\n")



beforeEach(function () {
  delete require.cache[require.resolve('../../server')]
  server = require('../../server')
})

afterEach(function (done) {
  server.close(done)
})

// configuracion de variables para produnfidad de pruebas

var resoluciones = []
var regiones = []
var filtros_sptarget = [] // fecha, rango y fosil
var ba_var = [] // bioticos y abioticos
var apr_prob = [] // apriori y probaildiad

if(nivel == "completo"){
  resoluciones = [8, 16, 32, 64, "state", "mun", "ageb", "cue"]
  regiones = [1 ,2 ,3 ,4, 5]
  filtros_sptarget = [["false", "false", "false"], ["false", "false", "true"], ["true", "true", "true"], ["true", "true", "false"], ["false", "true", "false"], ["false", "true", "true"], ["true", "false", "true"], ["true", "false", "false"]]
  ba_var = [["true", "false"],["false", "true"],["true", "true"]]
  apr_prob = [["false", "false"], ["true", "false"],["false", "true"], ["true", "true"]]
}
else if(nivel == "esencial"){
  resoluciones = [16]
  regiones = [1]
  filtros_sptarget = [["false", "false", "false"]]
  ba_var = [["true", "false"]]
  apr_prob = [["false", "false"]]
}
else{
  resoluciones = []
  regiones = []
  filtros_sptarget = []
  ba_var = []
  apr_prob = []
}




describe("Prueba: Acceso al Middleware",function(){

  it("Resultado: Middleware - OK", function(done){
    supertest(server).get("/niche/")
    .expect("Content-type",/json/)
    .expect(200)
    .end(function(err, response){
      response.statusCode.should.equal(200)
      done();
    })
  });

});


describe("Prueba: Carga inicial en árbol de variables abioticas",function(){

  it("Resultado: Árbol de variables abioticas - OK", function(done){

    supertest(server).get("/niche/especie/getRasterVariables")
    .send({footprint_region : 1, level: 0})
    .expect("Content-type",/json/)
    .expect(200)
    .end(function(err, response){
      response.statusCode.should.equal(200)
      expect(response.body.data).to.not.equal(null)
      expect(response.body.data).all.have.property("fuente")
      expect(response.body.data).all.have.property("type")
      done();
    })

  });

});



describe("Prueba: Navegación en árbol de variables abioticas",function(){

  it("Resultado: Navegación en árbol de variables abioticas - OK", function(done){

    supertest(server).get("/niche/especie/getRasterVariables")
    .send({region : 1, level: 1, field: "Worldclim", type: 2})
    .expect("Content-type",/json/)
    .expect(200)
    .end(function(err, response){
      response.statusCode.should.equal(200)
      expect(response.body.data).to.not.equal(null)
      expect(response.body.data).all.have.property("fuente")
      expect(response.body.data).all.have.property("type")
      expect(response.body.data).all.have.property("layer")
      expect(response.body.data).all.have.property("label")
      expect(response.body.data).all.have.property("coeficiente")
      expect(response.body.data).all.have.property("unidad")
      done();
    })

  });

});


describe("Prueba: Buscador de categorias taxonómicas",function(){

  it("Resultado: Buscador de categorias taxonómicas - OK", function(done){

    supertest(server).post("/niche/especie/getEntList")
    .send({nivel: "especieepiteto", searchStr: "lynx", source: 0, footprint_region: 1})
    .expect("Content-type",/json/)
    .expect(200)
    .end(function(err, response){
      response.statusCode.should.equal(200)
      // expect(response.body.data[0].generovalido).to.be.a("string")
      expect(response.body.data).all.have.property('especieepiteto');
      done();
    })

  });

});


describe("Prueba: Petición de mallas en diferentes resoluciones",function(){
  
  this.timeout(1000 * 60 * 3);

  
  // prueba de todas las resoluciones regulares e irregulares
  resoluciones.forEach(cell_res => {

    regiones.forEach(footprint_region => { // Se pueden agregar mas regiones

      it("Resultado: Petición de la malla de " + cell_res + " en la region: " + footprint_region + " - OK", 
      function(done){
        supertest(server).post("/niche/especie/getGridGeoJson")
        .send({grid_res : ""+cell_res, footprint_region:""+footprint_region})
        .expect("Content-type",/json/)
        .expect(200)
        .end(function(err, response){
          response.statusCode.should.equal(200)
          // expect(response.body.features.properties).all.have.property('gridid');
          response.res.text.includes('FeatureCollection').should.equal(true)
          response.res.text.includes('features').should.equal(true)
          done();
        })
      });

    });

  });
});


describe("Prueba: Navegación de árbol taxonómico (variables bióticas)", function(){
  
  it("Resultado: Navegación de árbol taxonómico - OK", function(done){

    supertest(server).post("/niche/especie/getVariables")
    .send({
          field:"phylumdivisionvalido",
          parentfield:"especievalidabusqueda",
          parentitem:"Lynx rufus",
        footprint_region:1
      })
    .expect("Content-type",/json/)
    .expect(200)
    .end(function(err, response){
      response.statusCode.should.equal(200)
      expect(response.body.data).all.have.property("name")
      expect(response.body.data).all.have.property("spp")
      done();
    });
  });

});



describe("Prueba: Obtiene nodos del análisis de comunidad", function(){

  this.timeout(1000 * 60 * 5); // 3 minutos maximo
	
	it("Resultado: Obtiene nodos del análisis de comunidad - OK", function(done){

		supertest(server).post("/niche/getTaxonsGroupNodes")
		.send({

            source:[{
          		biotic: true,
          		fGroupId: 1,
          		grp: 0,
          		level: "species",
          		rank: "genus",
          		type: 0,
          		value: "Aedes"
            }],
            target:[{
                biotic: true,
          		fGroupId: 2,
          		grp: 1,
          		level: "species",
          		rank: "genus",
          		type: 0,
          		value: "Lutzomyia"
            }],              
            date: true,
            footprint_region:1,
            fosil: true,
            min_occ:5,
            grid_res:"16",

        })
		.expect("Content-type",/json/)
		.expect(200)
		.end(function(err, response){
			response.statusCode.should.equal(200)
			expect(response.body.data).to.not.equal(null)
            expect(response.body.data).all.have.property("reinovalido")
            expect(response.body.data).all.have.property("phylumdivisionvalido")
            expect(response.body.data).all.have.property("clasevalida")
            expect(response.body.data).all.have.property("familiavalida")
            expect(response.body.data).all.have.property("generovalido")
            expect(response.body.data).all.have.property("familiavalida")
            expect(response.body.data).all.have.property("especieepiteto")
            expect(response.body.data).all.have.property("label")
            expect(response.body.data).all.have.property("occ")
            expect(response.body.data).all.have.property("coeficiente")
            expect(response.body.data).all.have.property("unidad")
            expect(response.body.data).all.have.property("biotic")
            expect(response.body.data).all.have.property("grp")
			done();
		});
	});
	
});




describe("Prueba: Obtiene los enlaces entre nodos del análisis de comunidad", function(){
	
	it("Resultado: Obtiene los enlaces entre nodos del análisis de comunidad - OK", function(done){

		this.timeout(1000 * 60 * 5); // 5 minutos maximo


		supertest(server).post("/niche/getTaxonsGroupEdges")
		.send({

            source:[{
          		biotic: true,
          		fGroupId: 1,
          		grp: 0,
          		level: "species",
          		rank: "genus",
          		type: 0,
          		value: "Aedes"
            }],
            target:[{
                biotic: true,
          		fGroupId: 2,
          		grp: 1,
          		level: "species",
          		rank: "genus",
          		type: 0,
          		value: "Lutzomyia"
            }],              
            date: true,
            footprint_region:1,
            fosil: true,
            min_occ: 5,
            grid_res:"16",

        })
		.expect("Content-type",/json/)
		.expect(200)
		.end(function(err, response){
			response.statusCode.should.equal(200)
			// expect(response.body.data).to.not.equal(null)
            expect(response.body.data).all.have.property("generovalido_s")
            expect(response.body.data).all.have.property("especieepiteto_s")
            expect(response.body.data).all.have.property("generovalido_t")
            expect(response.body.data).all.have.property("especieepiteto_t")
            expect(response.body.data).all.have.property("biotic_t")
            expect(response.body.data).all.have.property("biotic_s")
            expect(response.body.data).all.have.property("n")
            expect(response.body.data).all.have.property("ni")
            expect(response.body.data).all.have.property("nj")
            expect(response.body.data).all.have.property("nij")
            expect(response.body.data).all.have.property("value")
            expect(response.body.data).all.have.property("score")
			done();
		});
	});
	
});
