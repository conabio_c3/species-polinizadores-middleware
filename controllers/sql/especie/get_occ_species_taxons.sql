WITH spids_species AS (
	select spid
	FROM sp_snib 
	$<species_filter:raw>
	--WHERE (especievalidabusqueda = 'Zea mays')
	AND spid is not null
),
gids_countries AS (
	SELECT UNNEST(gid) AS gid 
	--FROM $<resolution_view:raw>
	from grid_geojson_16km_aoi
	--WHERE footprint_region=$<region:raw>
	WHERE footprint_region=1
)
select snib.aniocolecta as anio_colecta, snib.ejemplarfosil as es_fosil, 
-- not (snib.ejemplarfosil is null) as es_fosil, 
$<columns:raw>
from snib
join spids_species 
on spids_species.spid = snib.spid
join gids_countries 
on gids_countries.gid = snib.gid
WHERE 
	snib.spid is not null
	and the_geom is not null
	$<where_filter:raw>
	-- AND ( ( aniocolecta BETWEEN 1500 AND 2020 ) OR aniocolecta = 9999 ) OR ejemplarfosil = 'SI'
order by snib.aniocolecta