with biospp as (
	SELECT /*distinct clasevalida,
			ordenvalido,
			familiavalida,
			generovalido,
			especieepiteto,
			especievalidabusqueda*/
			count(*) as spp 
	FROM sp_snib
	WHERE 
		especieepiteto <> ''
		AND especievalidabusqueda <> ''
		-- AND array_length(cells_16km_1, 1) > 0
		-- AND array_length(cells_64km_1, 1) > 0
		AND array_length(cells_$<resolution:raw>km_$<region:raw>, 1) > 0
		AND ($<where_clause_bio:raw>)
		-- AND clasevalida = 'Mammalia'
),
abiospp as (
	SELECT 
	-- "type", layer, bid, label, icat, array_length(cells_64km::int[],1) 
	-- sum(array_length(cells_$<resolution:raw>km::int[],1))  as spp 
	count(*) as spp
	FROM raster_bins
	where $<where_clause_abio:raw>
	group by "type"
)
select COALESCE(biospp.spp,0) + COALESCE(abiospp.spp,0) as spp 
from biospp, abiospp