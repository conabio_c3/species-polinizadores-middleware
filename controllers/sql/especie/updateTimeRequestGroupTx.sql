UPDATE historicos_seleccion_gruposobj
SET 
contexto_analisis='$<contexto_analisis:raw>', 
rango_fecha=$<rango_fecha:raw>, 
reg_sfecha=$<reg_sfecha:raw>, 
reg_fosil=$<reg_fosil:raw>, 
fecha_inicio=$<fecha_inicio:raw>, 
fecha_fin=$<fecha_fin:raw>, 
num_celdas_spo=$<num_celdas_spo:raw>, 
iscarga_datos=$<iscarga_datos:raw>, 
tiempo_proceso_solicitud_grupotx=$<tiempo_proceso_solicitud_grupotx:raw>,
fecha_proceso_solicitud_grupotx= current_timestamp
WHERE 
id_grupoobj=$<id_registro:raw>
RETURNING id_grupoobj;