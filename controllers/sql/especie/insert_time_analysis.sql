INSERT INTO historicos_analisis
(	
	-- id_analisis incremental
	id_global_analisis, -- nuevo
	region, 
	resolucion, 
	num_poligonos_totales, -- nuevo
	num_celdas_spo, 
	rango_fecha, 
	reg_sfecha, 
	reg_fosil, 
	num_covars, 
	validacion, 
	num_iteraciones,
	min_occ, 
	apriori, 
	prob,
	bio, -- nuevo
	abio, -- nuevo
	contexto_analisis, -- nuevo
	tiempo_analisis,
	tiempo_proceso_analisis, -- nuevo
	fecha_registro, -- nuevo
	tiempo_parseo_analisis,
	fecha_solicitud,
	tipo_procedencia,
	entorno_db,
	entorno_fb
	)
VALUES(
	$<id_timestamp:raw>,  -- nuevo
	$<region:raw>, 
	'$<grid_resolution:raw>', 
	$<cell_region:raw>, -- nuevo
	$<cell_target:raw>, 
	$<date_range:raw>, 
	$<sdate:raw>, 
	$<fosil:raw>, 
	$<spp_covars:raw>, 
	$<validacion:raw>, 
	$<total_iterations:raw>, 
	$<min_occ:raw>, 
	$<apriori:raw>, 
	$<mapa_prob:raw>, 
	$<hasbio:raw>, -- nuevo
	$<hasabio:raw>, -- nuevo
	'$<contexto_analsis:raw>', -- nuevo
	$<analysis_time:raw>,
	$<process_time:raw>, -- nuevo
	current_timestamp, -- nuevo
	$<parsing_time:raw>, -- parsing_time
	'$<timestamp_request:raw>',
	'$<tipo_procedencia:raw>',
	'$<entorno_db:raw>',
	'$<entorno_fb:raw>'
) RETURNING id_analisis;
