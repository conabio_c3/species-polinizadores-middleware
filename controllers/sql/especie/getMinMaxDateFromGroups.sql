select min(aniocolecta) as mindate, max(aniocolecta) as maxdate 
from snib_grid_$<res:raw>km as b
join(
	select spid 
	FROM sp_snib
	where 
	$<where_clause:raw>
) as c
ON b.spid = c.spid
where b.spid is not null
and aniocolecta <> 9999