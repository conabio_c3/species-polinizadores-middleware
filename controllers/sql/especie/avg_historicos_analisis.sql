SELECT 
avg(fecha_display - fecha_solicitud) as avg_display
-- id_analisis, 
-- id_global_analisis, 
-- contexto_analisis, 
-- fecha_solicitud, fecha_registro, fecha_display,
-- tiempo_parseo_analisis, tiempo_analisis, tiempo_proceso_analisis, tiempo_display_analisis, 
-- bio, abio, 
-- region, resolucion, 
-- num_celdas_spo,  num_covars, num_poligonos_totales, 
-- rango_fecha, reg_sfecha, reg_fosil, min_occ, apriori, prob, num_iteraciones, validacion, 
-- tipo_procedencia, entorno_db, entorno_fb 
FROM historicos_analisis
where 
region = $<region:raw>
and resolucion = '$<grid_res:raw>'
-- and rango_fecha = $<rango_fecha:raw>
and reg_sfecha = $<sfecha:raw>
and reg_fosil = $<sfosil:raw>
-- and fecha_inicio = $<liminf:raw>
-- and fecha_fin = $<limsup:raw>
-- and iscarga_datos = 0
-- and num_celdas_spo = 309
-- and num_poligonos_totales = 9839
-- and es_solicitudmalla = 1
and tipo_procedencia = '$<tipo_procedencia:raw>'
and fecha_registro is not null
and fecha_solicitud is not null
and fecha_display is not null
-- order by fecha_solicitud desc
limit 10