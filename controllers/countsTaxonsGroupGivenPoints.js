var debug = require('debug')('verbs:countsTaxonsGroupGivenPoints')
var moment = require('moment')
var verb_utils = require('./verb_utils')
var queries = require('./sql/queryProvider')
var pgp = require('pg-promise')
var d3 = require('d3')
var config = require('../config')

var pool = verb_utils.pool 
var N = verb_utils.N 
var iterations = verb_utils.iterations
var alpha = verb_utils.alpha
var buckets = verb_utils.buckets
var default_region = verb_utils.region_mx
var max_score = verb_utils.maxscore
var min_score = verb_utils.minscore
var request_counter_map = d3.map([]);

var analysis_record_time;
const todaysDate = new Date()
const currentYear = todaysDate.getFullYear()

exports.countsTaxonsGroupGivenPoints = function(req, res, next) {

  debug('countsTaxonsGroupGivenPoints')

  var data_request = {}
  var data_target = {}
  var str_query = ''

  // Iniciando tiempo de ejecución
  analysis_record_time = process.hrtime();


  data_request["decil_selected"] = verb_utils.getParam(req, 'decil_selected', [10])

  var grid_resolution = verb_utils.getParam(req, 'grid_resolution', 16) 
  var region = parseInt(verb_utils.getParam(req, 'region', verb_utils.region_mx))
  var fosil = verb_utils.getParam(req, 'fosil', true)
  var date  = verb_utils.getParam(req, 'date', true)
  var lim_inf = verb_utils.getParam(req, 'lim_inf', 1500)
  var lim_sup = verb_utils.getParam(req, 'lim_sup', currentYear)
  var cells = verb_utils.getParam(req, 'excluded_cells', [])
  
  var cell_target = verb_utils.getParam(req, 'cell_target', 0)
  var cell_region = verb_utils.getParam(req, 'cell_region', 0)
  var id_timestamp = verb_utils.getParam(req, 'id_timestamp', 0)
  var timestamp_request = verb_utils.getParam(req, 'timestamp_request', 0)
  var tipo_procedencia = verb_utils.getParam(req, 'tipo_procedencia', 'UI')
  var entorno_fb = verb_utils.getParam(req, 'entorno_fb', 'db_dev')

  // var target_group = verb_utils.getParam(req, 'target_taxons', []) 

  debug("grid_resolution: " + grid_resolution)
  debug("cell_target: " + cell_target)
  debug("id_timestamp: " + id_timestamp)
  debug("cell_region: " + cell_region)


  data_request['lim_inf'] = lim_inf
  data_request['lim_sup'] = lim_sup
  data_request["excluded_cells"] = cells
  data_request["fosil"] = fosil
  data_request["date"] = date
  data_request["region"] = region
  data_request["grid_resolution"] = grid_resolution
  data_request["res_celda"] = "cells_"+grid_resolution+"km"
  data_request["res_celda_sp"] = "cells_"+grid_resolution+"km_"+region 
  data_request["res_celda_snib"] = "gridid_"+grid_resolution+"km" 
  data_request["res_celda_snib_tb"] = "grid_geojson_" + grid_resolution + "km_aoi"
  data_request["res_grid_tbl"] = "grid_" + data_request.grid_resolution + "km_aoi"
  data_request["min_occ"] = verb_utils.getParam(req, 'min_cells', 1)
  data_request["cell_target"] = cell_target
  data_request["id_timestamp"] = id_timestamp
  data_request["cell_region"] = cell_region
  data_request["timestamp_request"] = timestamp_request
  data_request["tipo_procedencia"] = tipo_procedencia
  data_request["entorno_fb"] = entorno_fb

  data_request["target_name"] = verb_utils.getParam(req, 'target_name', 'target_group')
  
  var covars_groups = verb_utils.getParam(req, 'covariables', []) 
  
  // debug("**********************")
  // debug(covars_groups[0].merge_vars[0])
  // debug("**********************")

  data_request["contexto_analsis"] = "fuente: [{carga_archivo}] covars: "+  JSON.stringify(covars_groups)

  var items_covars = [];
  covars_groups.forEach(function(item){
    item.merge_vars.forEach(function(merge_var){
      items_covars.push({nivel_taxon:merge_var.rank, value_taxon: merge_var.value})
    });
  });

  debug(items_covars)  


  data_request["alpha"] = undefined
  data_request["idtabla"] = verb_utils.getParam(req, 'idtabla', "")
  data_request["get_grid_species"] = verb_utils.getParam(req, 'get_grid_species', false)
  data_request["apriori"] = verb_utils.getParam(req, 'apriori', false)
  data_request["mapa_prob"] = verb_utils.getParam(req, 'mapa_prob', false)
  data_request["long"] = verb_utils.getParam(req, 'longitud', 0)
  data_request["lat"] = verb_utils.getParam(req, 'latitud', 0)
  data_request["title_valor"] = {'title': data_request["target_name"]}
  data_request["with_data_freq"] = verb_utils.getParam(req, 'with_data_freq', true)
  data_request["with_data_score_cell"] = verb_utils.getParam(req, 'with_data_score_cell', true)
  data_request["with_data_freq_cell"] = verb_utils.getParam(req, 'with_data_freq_cell', true)
  data_request["with_data_score_decil"] = verb_utils.getParam(req, 'with_data_score_decil', true)
  data_request["target_points"] = verb_utils.getParam(req, 'target_points', [])


  // debug("**********************")
  // debug(data_request["target_points"])
  // debug("**********************")


  var NIterations = verb_utils.getParam(req, 'iterations', iterations)
  var iter = 0
  var json_response = {}

  debug('*********** Tiempo despues de obtener parametros: ' + verb_utils.parseHrtimeToSeconds(process.hrtime(analysis_record_time)) + 'segundos');

  // debug("iteraciones: " + NIterations)


  // TODO: Falta definir estas variables de manera correcta
  // data_request["bio"] = 0;
  // data_request["abio"] = 0;
  // data_request["spp_covars"] = 1;

  pool.task(t => {

    var resp_util = verb_utils.getSppQuery(items_covars);
    debug(resp_util)
    
    data_request["bio"] = resp_util.bio;
    data_request["abio"] = resp_util.abio;

    debug("************* where_clause_covars Bio:" + resp_util.whereClauseBio)
    debug("************* where_clause_covars Abio:" + resp_util.whereClauseAbio)

    var query = "";

    if(resp_util.bio == 1 && resp_util.abio == 1){
      query = queries.getVariablesNiche.getSpp;
    }
    else if(resp_util.bio == 1){
      query = queries.getVariablesNiche.getSppBio;
    }
    else{
      query = queries.getVariablesNiche.getSppAbio;
    }

    debug("************* query:" + query)

    return t.one(query, {
      where_clause_bio: resp_util.whereClauseBio,
      where_clause_abio: resp_util.whereClauseAbio,
      region: region,
      resolution: grid_resolution
    }).then(resp => {

      // Obtiene respuesta covars y almacenar en data_request
      debug("resp.spp covars:" + resp.spp)
      data_request["spp_covars"] = resp.spp;

      var query = queries.subaoi.getCountriesRegion  

      return t.one(query, data_request).then(resp => {

        data_request["gid"] = resp.gid
        
        debug("Iteraciones: " + NIterations)

        for(var iter = 0; iter<NIterations; iter++){

          initialProcess(iter, NIterations, data_request, res, json_response, req, covars_groups)

        }

      })


    })

  })

}


function initialProcess(iter, total_iterations, data, res, json_response, req, covars_groups) {

  debug('initialProcess')
  debug('iter:' + (iter + 1))

  var data_request = JSON.parse(JSON.stringify(data))
  // debug('resolution: ' + data_request["grid_resolution"])

  var points = '['
  var number_occ = 0

  data_request["target_points"].forEach(function(occ) {
  	
  	if(data_request["fosil"]){

	  	if(data_request["date"]){

        if((occ['anio'] >= data_request['lim_inf'] && occ['anio'] <= data_request['lim_sup']) || occ['anio'] == 9999){
          
          if(number_occ > 0) {
            points += ', '
          }  

          points += 'ST_SetSRID('+ 'ST_Point('+ occ['longitud'] + ', ' + occ['latitud'] +')' +', 4326)'
          number_occ += 1

        }
  			

  		} else { 

  		  	if(occ['anio'] >= data_request['lim_inf'] && occ['anio'] <= data_request['lim_sup']){

  		  		if(number_occ > 0) {
  			  		points += ', '
  			  	} 

  		  		points += 'ST_SetSRID('+ 'ST_Point('+ occ['longitud'] + ', ' + occ['latitud'] +')' +', 4326)'
  		  		number_occ += 1
  		  	
  		  	}

		}

	} else {

    // debug("sin fosiles")

    	if( occ['fosil'] == "NO" || occ['fosil'] == "" ){

	  		if(data_request["date"]){

          // debug("date true" )

  				if((occ['anio'] >= data_request['lim_inf'] && occ['anio'] <= data_request['lim_sup']) || occ['anio'] == 9999){
          
            if(number_occ > 0) {
              points += ', '
            }  

            points += 'ST_SetSRID('+ 'ST_Point('+ occ['longitud'] + ', ' + occ['latitud'] +')' +', 4326)'
            number_occ += 1

          }

  			} else {

  			  	if(occ['anio'] >= data_request['lim_inf'] && occ['anio'] <= data_request['lim_sup']){

  			  		if(number_occ > 0) {
  				  		points += ', '
  				  	}  

  			  		points += 'ST_SetSRID('+ 'ST_Point('+ occ['longitud'] + ', ' + occ['latitud'] +')' +', 4326)'
  			  		number_occ += 1
  			  	
  			  	}

			 }

	  	}  

	}

  });

  points += ']'


  // debug(points)
  
  pool.task(t => {

    var query = queries.countsTaxonGroups.getCellSincePoint

    const query1 = pgp.as.format(query, {

    	res: data_request["grid_resolution"],
    	points: points

    })
    //debug(query1)

    return t.any(query, {

    	res: data_request["grid_resolution"],
    	points: points

    }).then(data => {

      var resp = {target_cells:[]}

      data.forEach(item => {
        resp['target_cells'].push(item['gridid'])
      });

      debug(resp['target_cells'].length) // Numero de puntos dado por el usuario

      const unique_set = new Set(resp["target_cells"])

      data_request["target_cells"] = Array.from(unique_set)

      debug(data_request["target_cells"].length)  // numero de celdas únicas    

      
     var query = data_request.idtabla === "" ? "select array[]::integer[] as total_cells" : queries.validationProcess.getTotalCells

     return t.one(query, {

          tbl_process: data_request.idtabla,
          iter: (iter+1)

      }).then(resp => { 

        data_request["total_cells"] = resp.total_cells

        var query = data_request.idtabla === "" ? "select array[]::integer[] as source_cells" : queries.validationProcess.getSourceCells

        return t.one(query, {
        
          tbl_process: data_request.idtabla,
          iter: (iter+1),
          res_grid_tbl: data_request.res_grid_tbl,
          res_grid_column: data_request.res_celda_snib

        }).then(resp => {

          // debug(resp.source_cells)

          data_request["source_cells"] = resp.source_cells

          return t.one(queries.basicAnalysis.getN, {

                grid_resolution: data_request.grid_resolution,
                footprint_region: data_request.region

          })

        })
      }).then(resp => {

        data_request["N"] = resp.n 
        data_request["alpha"] = data_request["alpha"] !== undefined ? data_request["alpha"] : 1.0/resp.n

        var query_analysis = queries.countsTaxonGroups.getCountsBaseGivenPoints
        data_request['groups'] = verb_utils.getCovarGroupQueries(queries, data_request, covars_groups)

        if( data_request["get_grid_species"] !== false ) {

          debug("analisis en celda")

          debug("long: " + data_request.long)
          debug("lat: " + data_request.lat)

          data_temp = {
            'res_celda_snib'    : data_request.res_celda_snib, 
            'res_celda_snib_tb' : data_request.res_grid_tbl,
            'long'              : data_request.long,
            'lat'               : data_request.lat
          }

          const query1 = pgp.as.format(queries.basicAnalysis.getGridIdByLatLong, data_temp)
          // debug("iter " + iter + query1)

          return t.one(queries.basicAnalysis.getGridIdByLatLong, data_temp).then(resp => {

                data_request["cell_id"] = resp.gridid
                debug("cell_id: " + data_request.cell_id)
                
                return t.any(query_analysis, data_request)  

          })

        } else {

          debug("analisis general")

          data_request["cell_id"] = 0

          if(JSON.parse(data_request.apriori) === true || JSON.parse(data_request.mapa_prob) === true) {


            return t.one(queries.basicAnalysis.getAllGridId, data_request).then(data => {

              data_request.all_cells = data
              return t.any(query_analysis, data_request)

            })


          } else {

            debug('--------------------------------------------------')
            debug("analisis basico")

            var analysis_parsing_time = verb_utils.parseHrtimeToSeconds(process.hrtime(analysis_record_time))

            debug('*********** Tiempo antes de ejecutar análisis: ' + analysis_parsing_time + 'segundos');

            data_request["tiempo_parseo"] = analysis_parsing_time

            //const query1 = pgp.as.format(query_analysis, data_request)
            //debug(query1)

            return t.any(query_analysis, data_request)

           }

         }

      })
    
    }).then(data_iteration => {

      debug('*********** Tiempo despues de ejecutar análisis iteracion: ' + verb_utils.parseHrtimeToSeconds(process.hrtime(analysis_record_time)) + 'segundos');
      

      var decil_selected = data_request["decil_selected"]
      // debug(data_iteration)
    
      var data_response = {iter: (iter+1), data: data_iteration, test_cells: data_request["source_cells"], target_cells: data_request["target_cells"], apriori: data_request.apriori, mapa_prob: data_request.mapa_prob }
      json_response["data_response"] = json_response["data_response"] === undefined ? [data_response] : json_response["data_response"].concat(data_response)


      
      if(!request_counter_map.has(data_request["title_valor"].title)){

        request_counter_map.set(data_request["title_valor"].title, 1)

      } else {

        var count = request_counter_map.get(data_request["title_valor"].title);
        request_counter_map.set(data_request["title_valor"].title, count+1)
      
      }    


      // TODO: Verificar este flujo no se maneja
      debug("*******************************")
      debug("title_valor: " + data_request["title_valor"].title)
      debug("request_counter_map: " + request_counter_map.get(data_request["title_valor"].title))
      debug("total_iterations: " + total_iterations)
      debug("*******************************")

    
      if(request_counter_map.get(data_request["title_valor"].title) === total_iterations){

          // tiempo despues de proceso de query
          var analysis_time = verb_utils.parseHrtimeToSeconds(process.hrtime(analysis_record_time))


          request_counter_map.set(data_request["title_valor"].title, 0)

          debug("COUNT PROCESS FINISHED")
          var data = []
          var data_avg = []
          var validation_data = []
          var is_validation = false
          var data_freq = []
          var data_score_cell = []
          var data_freq_cell = []
          var percentage_occ = []
          var decil_cells = []


          if(data_iteration.length != 0){


              if(total_iterations !== 1){

                  debug("PROCESS RESULTS FOR VALIDATION")
                  is_validation = true

                  // Promedia los valores obtenidos en las N iteraciones para n, nj, nij, ni, epsilon y score. 
                  // Además obtiene un array de cobertura total por las celdas de cada especie

                  var dup_array = JSON.parse(JSON.stringify(json_response["data_response"]))

                  data = verb_utils.processGroupValidationData(dup_array)

                  // Obtiene los 20 rangos de epsilon y score por especie, utilizados para las gráficas en el cliente de frecuencia por especie. 
                  // En caso de ser validación se promedia cada rango

                  data_freq = data_request.with_data_freq === true ? verb_utils.processDataForFreqSpecie(json_response["data_response"], is_validation) : []

                  validation_data = data_request.with_data_score_decil === true ? verb_utils.getValidationValues(json_response["data_response"]) : []


              } else{

                  debug("PROCESS RESULTS")
                  is_validation = false

                  data = data_iteration

                  validation_data = data_request.with_data_score_decil === true ? verb_utils.getValidationDataNoValidation(data, 
                                    data_request["target_cells"],
                                    data_request["res_celda_snib"], 
                                    data_request["where_target"], 
                                    data_request["res_celda_snib_tb"], 
                                    data_request["region"], 
                                    data_request["res_celda_sp"], 
                                    data_request["apriori"],
                                    data_request["mapa_prob"],
                                    queries) : []

                  
                  // Obtiene los 20 rangos de epsilon y score por especie, utilizados para las gráficas en el cliente de frecuencia por especie. 
                  // En caso de ser validación se promedia cada rango
                  data_freq = data_request.with_data_freq === true ? verb_utils.processDataForFreqSpecie([data], is_validation) : []

                
              }



              var apriori = false
              debug("data_request.apriori: " + data_request.apriori)
              if(data_request.apriori !== false && data[0].ni !== undefined){
                apriori = true
              }

              var mapa_prob = false
              debug("data_request.mapa_prob: " + data_request.mapa_prob)
              if(data_request.mapa_prob !== false && data[0].ni !== undefined){
                mapa_prob = true          
              }


              // TODO: Revisar comportamiento con seleccion de celda
              // data = is_validation ? data_avg : data

              var cell_id = 0
              if(data_request.get_grid_species !== false){

                  cell_id = data_request.cell_id
                  debug("cell_id last: " + cell_id)
                  data = verb_utils.processGroupDataForCellId(data, apriori, mapa_prob, cell_id)

              }



              debug("COMPUTE RESULT DATA FOR HISTOGRAMS")
              

              // Obtiene la sumatoria de score por celdas contemplando si existe apriori o probabilidad
              data_score_cell = data_request.with_data_score_cell === true ? verb_utils.processDataForScoreCell(data, apriori, mapa_prob, data_request.all_cells, is_validation) : []


              // TODO: Revisar funcionamiento con validacion
              data_freq_cell = data_request.with_data_freq_cell === true ? verb_utils.processDataForFreqCell(data_score_cell) : []



              // Obtiene el score por celda, asigna decil y Obtiene la lista de especies por decil seleccionado en las N iteraciones requeridas
              // data = is_validation ? verb_utils.processCellDecilPerIter(json_response["data_response"], apriori, mapa_prob, data_request.all_cells, is_validation) : data
              
              if(data_request.with_data_score_decil === true ){

                  debug("Calcula valores decil")

                  var decilper_iter = verb_utils.processCellDecilPerIter(json_response["data_response"], apriori, mapa_prob, data_request.all_cells, is_validation, decil_selected) 
                  percentage_occ = decilper_iter.result_datapercentage
                  decil_cells = decilper_iter.decil_cells

              }

            
          }
          else{
            debug("****************+")
            debug("ANALISIS SIN DATOS")
            debug("****************+")
          }


          // tiempo despues de proceso de respuesta de query
          var analysis_process_time = verb_utils.parseHrtimeToSeconds(process.hrtime(analysis_record_time))

          debug('*********** Tiempo despues de ejecutar análisis TOTAL iteraciones: ' + analysis_time + 'segundos');


          var date_range = 1;
          if(data_request["lim_inf"] == 1500 && data_request["lim_sup"] === currentYear){
            date_range = 0;
          }
          var validacion = total_iterations > 1;

          // TODO: Hacer inserción de tiempo en la base
          debug("region: " + data_request["region"])
          debug("grid_resolution: " + data_request["grid_resolution"])
          debug("apriori: " + data_request["apriori"] ? 1:0)
          debug("mapa_prob: " + data_request["mapa_prob"] ? 1:0)
          debug("fosil: " + data_request["fosil"])
          debug("date: " + data_request["date"])
          debug("total_iterations: " + total_iterations) // validacion > 1
          debug("validacion: " + validacion) 
          debug("min_occ: " + data_request["min_occ"])
          debug("lim_inf: " + data_request["lim_inf"])
          debug("lim_sup: " + data_request["lim_sup"])
          debug("date_range: " + date_range)
          // debug("spp_target: " + data_request["spp_target"])
          debug("spp_covars: " + data_request["spp_covars"])
          
          debug("tiempo_parseo: " + parseFloat(data_request["tiempo_parseo"]))
          debug("analysis_time: " + parseFloat(analysis_time))
          debug("tiempo_proceso: " + parseFloat(analysis_process_time))

          debug("cell_target: " + data_request["cell_target"])
          debug("bio: " + data_request["bio"])
          debug("abio: " + data_request["abio"])
          debug("contexto_analsis: " + data_request["contexto_analsis"])
          debug("cell_region: " + data_request["N"])
          debug("timestamp_request: " + data_request["timestamp_request"])
          debug("tipo_procedencia: " + data_request["tipo_procedencia"])
          debug("config.db.database: " + config.db.database)
          debug("entorno_fb: " + data_request["entorno_fb"])


          var query = queries.getVariablesNiche.setTimeAnalysis;
          // debug("query:" + query)

          pool.task(t => {

            return t.any(query, {

              region: data_request["region"],
              grid_resolution: data_request["grid_resolution"],
              // spp_target: data_request["spp_target"],
              date_range: date_range,
              sdate: data_request["date"] ? 1 : 0,
              fosil: data_request["fosil"] ? 1 : 0,
              spp_covars: data_request["spp_covars"],
              validacion: data_request["validacion"] ? 1 : 0,
              min_occ: data_request["min_occ"],
              apriori: data_request["apriori"] ? 1 : 0,
              mapa_prob: data_request["mapa_prob"] ? 1 : 0,
              total_iterations: total_iterations,
              analysis_time: parseFloat(analysis_time),
              cell_target: data_request["cell_target"],
              id_timestamp: data_request["id_timestamp"],
              hasbio: data_request["bio"],
              hasabio: data_request["abio"],
              contexto_analsis: data_request["contexto_analsis"],
              cell_region: data_request["N"],
              parsing_time: parseFloat(data_request["tiempo_parseo"]),
              process_time: parseFloat(analysis_process_time),
              timestamp_request: data_request["timestamp_request"],
              tipo_procedencia: data_request["tipo_procedencia"],
              entorno_db: config.db.database,
              entorno_fb: data_request["entorno_fb"]
              
            }).then(resp => {

              debug(resp);
              var id_analisis = resp[0].id_analisis;

              request_counter_map.set(data_request["title_valor"].title, 0)

              debug("****************************************")
              debug("RESPUESTA ENVIADA")
              debug("****************************************")

              debug('*********** Tiempo despues de ejecutar y procesar análisis FINAL: ' + verb_utils.parseHrtimeToSeconds(process.hrtime(analysis_record_time)) + 'segundos');

              res.json({
                  ok: true,
                  data: data,
                  data_freq: data_freq,
                  data_score_cell: data_score_cell,
                  data_freq_cell: data_freq_cell,
                  validation_data: validation_data,
                  percentage_avg: percentage_occ,
                  decil_cells: decil_cells,
                  id_unico: id_analisis,
                  id_global: data_request["id_timestamp"],
                  final_time: analysis_process_time
              })

            })

          })



          // debug("****************************************")
          // debug("RESPUESTA ENVIADA")
          // debug("****************************************")

          // res.json({
          //     ok: true,
          //     data: data,
          //     data_freq: data_freq,
          //     data_score_cell: data_score_cell,
          //     data_freq_cell: data_freq_cell,
          //     validation_data: validation_data,
          //     percentage_avg: percentage_occ,
          //     decil_cells: decil_cells
          // })


        
      }
      else{

        debug("***************** action no manejada")


      }

    }).catch(error => {

      throw error

    	debug("ERROR EN PROMESA: " + error)

      res.json({
          ok: false,
          message: "Error al ejecutar la petición",
          data:[],
          data_freq: [],
          data_score_cell: [],
          data_freq_cell: [],
          validation_data: [],
          percentage_avg: [],
          decil_cells: [],
          error: error
        })

    });
  
  });
}