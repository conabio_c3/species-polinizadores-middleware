/**
* @module controllers/getCountsTaxonsGroup
* @requires debug
* @requires pg-promise
* @requires moment
* @requires config
* @requires module:controllers/verb_utils
* @requires module:controllers/sql/queryProvider
**/
var debug = require('debug')('verbs:getCountsTaxonsGroup')
var moment = require('moment')
var verb_utils = require('./verb_utils')
var queries = require('./sql/queryProvider')
var pgp = require('pg-promise')
var d3 = require('d3')

var pool = verb_utils.pool


exports.updateTimeAnalysis = function(req, res, next) {

	debug('updateTimeAnalysis')

	var idunico = verb_utils.getParam(req, 'idunico', 0)
	var idglobal = verb_utils.getParam(req, 'idglobal', 0)
	var tiempoproceso = verb_utils.getParam(req, 'tiempoproceso', 0)
	var start_time = verb_utils.getParam(req, 'start_time', 0)
	var end_time = verb_utils.getParam(req, 'end_time', 0)
	var timestamp_display = verb_utils.getParam(req, 'timestamp_display', 0)

	debug('idunico:' + idunico)
	debug('idglobal:' + idglobal)
	debug('tiempoproceso:' + tiempoproceso)
	debug('start_time:' + start_time)
	debug('end_time:' + end_time)
	debug('timestamp_display:' + timestamp_display)

	var final_time = parseFloat(tiempoproceso) + (parseFloat(end_time) - parseFloat(start_time));
	debug('final_time:' + final_time)

	pool.any(queries.getVariablesNiche.updateTimeAnalysis, { 
		idunico: idunico,
		idglobal: idglobal,
		final_time: final_time,
		timestamp_display: timestamp_display
  	})
    .then(function (data) {
      res.json({'data': data})
    })
    .catch(function (error) {
      next(error)
    })

}


exports.updateTimeDislayTaxonGroup = function(req, res, next) {

	debug('updateTimeDislayTaxonGroup')

	var id_registro       		= verb_utils.getParam(req, 'id_registro', 0)
	var timestamp_request       = verb_utils.getParam(req, 'timestamp_request', 0)
    var endtimedisplaytinitgrid = verb_utils.getParam(req, 'endtimedisplaytinitgrid', 0) 

	debug('id_registro:' + id_registro)
	debug('timestamp_request:' + timestamp_request)
	debug('endtimedisplaytinitgrid:' + endtimedisplaytinitgrid)

	var query = queries.getSpeciesNiche.updateTimeDisplayRequestGroupTx;

	pool.any(query, { 
		id_registro: id_registro,
		timestamp_request: timestamp_request,
		endtimedisplaytinitgrid: endtimedisplaytinitgrid
  	})
    .then(function (data) {
      
      res.json({
      	'data': data
      })
      
    })
    .catch(function (error) {

    	debug(error)
      
	      return res.json({
	        err: error,
	        ok: false,
	        message: "Error al procesar la query"
	      })

    })

}


