SELECT /*distinct clasevalida,
		ordenvalido,
		familiavalida,
		generovalido,
		especieepiteto,
		especievalidabusqueda*/
		count(*) as spp 
FROM sp_snib
WHERE 
	especieepiteto <> ''
	AND especievalidabusqueda <> ''
	-- AND array_length(cells_16km_1, 1) > 0
	-- AND array_length(cells_64km_1, 1) > 0
	AND array_length(cells_$<resolution:raw>km_$<region:raw>, 1) > 0
	AND ($<where_clause_bio:raw>)
	-- AND clasevalida = 'Mammalia'