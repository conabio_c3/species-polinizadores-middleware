// Configuration file for middleware
var config = {}


config.db = {}
config.db.database = process.env.DBNAME
config.db.user = process.env.DBUSER
config.db.password = process.env.DBPWD
config.db.host = process.env.DBHOST
config.db.port = process.env.DBPORT
config.db.application_name = 'expressMiddleware'
config.port = process.env.PORT
config.email = {}
config.email.user = process.env.EUSER
config.email.pass = process.env.EPASS
config.email.host = process.env.EHOST
config.email.port = process.env.EPORT

config.test = {}
config.test.ambiente = "prod"
config.test.db = config.db.database
config.test.profundidad = "completo"

// Configure pool of connections
config.db.poolSize = 10

config.SEED = "@hardseedconabio2018";
config.TIME_TOKEN = 14400;

module.exports = config

