var supertest = require("supertest");
var should = require("should");
var expect = require('chai').expect;
var moment = require('moment')

var chai = require("chai");
// chai.should();
chai.use(require('chai-things'));

var config = require('../../config')


var server;
var ambiente = config.test.ambiente;
var base = config.test.db;
var nivel = config.test.profundidad;

// TODO: sets para pruebas con diferentes especies
var sets = [{}];

console.info("************************************************\n")
console.info("\tINICIO DE PRUEBAS EN NICHO\n")
console.info("\tAmbiente: " + ambiente  + "\n")
console.info("\tBase: " + base  + "\n")
console.info("\tProfundidad: " + nivel  + "\n")
console.info("************************************************\n")

beforeEach(function () {
	delete require.cache[require.resolve('../../server')]
	server = require('../../server')
})

afterEach(function (done) {
	server.close(done)
})



// configuracion de variables para produnfidad de pruebas

var resoluciones = []
var regiones = []
var filtros_sptarget = [] // fecha, rango y fosil
var ba_var = [] // bioticos y abioticos
var apr_prob = [] // apriori y probaildiad

if(nivel == "completo"){
	resoluciones = [8, 16, 32, 64, "state", "mun", "ageb", "cue"]
	regiones = [1 ,2 ,3 ,4, 5]
	filtros_sptarget = [["false", "false", "false"], ["false", "false", "true"], ["true", "true", "true"], ["true", "true", "false"], ["false", "true", "false"], ["false", "true", "true"], ["true", "false", "true"], ["true", "false", "false"]]
	ba_var = [["true", "false"],["false", "true"],["true", "true"]]
	apr_prob = [["false", "false"], ["true", "false"],["false", "true"], ["true", "true"]]
}
else if(nivel == "esencial"){
	resoluciones = [16]
	regiones = [1]
	filtros_sptarget = [["false", "false", "false"]]
	ba_var = [["true", "false"]]
	apr_prob = [["false", "false"]]
}
else{
	resoluciones = []
	regiones = []
	filtros_sptarget = []
	ba_var = []
	apr_prob = []
}




describe("Prueba: Acceso al Middleware",function(){

	it("Resultado: Middleware - OK", function(done){
		supertest(server).get("/niche/")
		.expect("Content-type",/json/)
		.expect(200)
		.end(function(err, response){
			response.statusCode.should.equal(200)
			done();
		})
	});

});


describe("Prueba: Carga inicial en árbol de variables abioticas",function(){

	it("Resultado: Árbol de variables abioticas - OK", function(done){

		supertest(server).get("/niche/especie/getRasterVariables")
		.send({footprint_region : 1, level: 0})
		.expect("Content-type",/json/)
		.expect(200)
		.end(function(err, response){
			response.statusCode.should.equal(200)
			expect(response.body.data).to.not.equal(null)
			expect(response.body.data).all.have.property("fuente")
			expect(response.body.data).all.have.property("type")
			done();
		})

	});

});



describe("Prueba: Navegación en árbol de variables abioticas",function(){

	it("Resultado: Navegación en árbol de variables abioticas - OK", function(done){

		supertest(server).get("/niche/especie/getRasterVariables")
		.send({region : 1, level: 1, field: "Worldclim", type: 2})
		.expect("Content-type",/json/)
		.expect(200)
		.end(function(err, response){
			response.statusCode.should.equal(200)
			expect(response.body.data).to.not.equal(null)
			expect(response.body.data).all.have.property("fuente")
			expect(response.body.data).all.have.property("type")
			expect(response.body.data).all.have.property("layer")
			expect(response.body.data).all.have.property("label")
			expect(response.body.data).all.have.property("coeficiente")
			expect(response.body.data).all.have.property("unidad")
			done();
		})

	});

});



describe("Prueba: Buscador de categorias taxonómicas",function(){

	it("Resultado: Buscador de categorias taxonómicas - OK", function(done){

		supertest(server).post("/niche/especie/getEntList")
		.send({nivel: "especieepiteto", searchStr: "lynx", source: 0, footprint_region: 1})
		.expect("Content-type",/json/)
		.expect(200)
		.end(function(err, response){
			response.statusCode.should.equal(200)
			// expect(response.body.data[0].generovalido).to.be.a("string")
			expect(response.body.data).all.have.property('especieepiteto');
			done();
		})

	});

});



describe("Prueba: Petición de mallas en diferentes resoluciones",function(){
	
	this.timeout(1000 * 60 * 3);

	
	// prueba de todas las resoluciones regulares e irregulares
	resoluciones.forEach(cell_res => {

		regiones.forEach(footprint_region => { // Se pueden agregar mas regiones

			it("Resultado: Petición de la malla de " + cell_res + " en la region: " + footprint_region + " - OK", 
			function(done){
				supertest(server).post("/niche/especie/getGridGeoJson")
				.send({grid_res : ""+cell_res, footprint_region:""+footprint_region})
				.expect("Content-type",/json/)
				.expect(200)
				.end(function(err, response){
					response.statusCode.should.equal(200)
					// expect(response.body.features.properties).all.have.property('gridid');
					response.res.text.includes('FeatureCollection').should.equal(true)
					response.res.text.includes('features').should.equal(true)
					done();
				})
			});

		});

	});
});



filtros_sptarget.forEach(pair => {
// [["false", "false", "false"], ["false", "false", "true"], ["true", "true", "true"], ["true", "true", "false"], ["false", "true", "false"], ["false", "true", "true"], ["true", "false", "true"], ["true", "false", "false"]].forEach(pair => {
// [["false", "false", "false"]].forEach(pair => {

	describe("Prueba: Obtención de celdas del grupo objetivo, variando fecha, rango y fosil", function(done){

		this.timeout(1000 * 60 * 2); // 3 minutos maximo
		
		var lim_inf = 1500
		var lim_sup = parseInt(moment().format('YYYY'));

		if(pair[1] === "false"){
			lim_inf = 2000
			lim_sup = 2020
		}

		var target_taxons = [{
			nivel: 8,
			taxon_rank: "species",
			title: "test_group",
			value: "Lynx rufus"
		}]

		describe("Prueba: fecha | rango | fosil => " + pair,function(){

			it("Resultado: fecha | rango | fosil => " + pair + " - OK", function(done){

				supertest(server).post("/niche/especie/getGridSpeciesTaxon")
				.send({
					target_taxons: target_taxons,
					name: "k",
					sfecha: pair[0], 
					sfosil: pair[2],
					liminf: lim_inf,
					limsup: lim_sup,
					idtime: "1549052020331",
					grid_res: 16,
					region: 1
				})
				.expect("Content-type",/json/)
				.expect(200)
				.end(function(err, response){
					response.statusCode.should.equal(200)
					expect(response.body.data).all.have.property("gridid")
					expect(response.body.data).all.have.property("occ")
					done();
				})

			});


		})

	});

});



filtros_sptarget.forEach(pair => {
// [["false", "false", "false"], ["false", "false", "true"], ["true", "true", "true"], ["true", "true", "false"], ["false", "true", "false"], ["false", "true", "true"], ["true", "false", "true"], ["true", "false", "false"]].forEach(pair => {
// [["false", "false", "false"]].forEach(pair => {

	describe("Prueba: Obtención de celdas del grupo objetivo por carga de archivo, variando fecha, rango y fosil", function(done){

		this.timeout(1000 * 60 * 2); // 3 minutos maximo
		
		var lim_inf = 1500
		var lim_sup = parseInt(moment().format('YYYY'));
		
		if(pair[1] === "false"){
			lim_inf = 2000
			lim_sup = 2020
		}

		var target_points = [{
			anio: 9999,
			fosil: "NO",
			latitud: 16.799892,
			longitud: -93.266542
		},
		{
			anio: 9999,
			fosil: "NO",
			latitud: 16.93772,
			longitud: -93.30668
		},
		{
			anio: 9999,
			fosil: "NO",
			latitud: 15.530939,
			longitud: -105.082803
		}]

		describe("Prueba: Archivo, fecha | rango | fosil => " + pair,function(){

			it("Resultado: Archivo, fecha | rango | fosil => " + pair + " - OK", function(done){

				supertest(server).post("/niche/especie/getGridGivenPoints")
				.send({
					target_points: target_points,
					date: pair[0], 
					fosil: pair[2],
					lim_inf: lim_inf,
					lim_sup: lim_sup,
					idtime: "1549052020331",
					grid_resolution: 16,
					region: 1
				})
				.expect("Content-type",/json/)
				.expect(200)
				.end(function(err, response){
					response.statusCode.should.equal(200)
					expect(response.body.data).all.have.property("gridid")
					expect(response.body.data).all.have.property("occ")
					done();
				})

			});


		})

	});

});



filtros_sptarget.forEach(pair => {
// [["false", "false", "false"], ["false", "false", "true"], ["true", "true", "true"], ["true", "true", "false"], ["false", "true", "false"], ["false", "true", "true"], ["true", "false", "true"], ["true", "false", "false"]].forEach(pair => {
// [["false", "false", "false"]].forEach(pair => {

	describe("Prueba: Selección de celda en mapa de especie objetivo, variando fecha, rango y fosil", function(done){

		this.timeout(1000 * 60 * 2); // 3 minutos maximo
		
		var lim_inf = 1500
		var lim_sup = parseInt(moment().format('YYYY'));
		
		if(pair[1] === "false"){
			lim_inf = 2000
			lim_sup = 2020
		}

		var target_taxons = [{
			nivel: 8,
			taxon_rank: "species",
			title: "linces",
			value: "Lynx rufus"
		}]

		

		describe("Prueba: fecha | rango | fosil => " + pair,function(){

			it("Resultado: fecha | rango | fosil => " + pair + " - OK", function(done){

				supertest(server).post("/niche/especie/getCellOcurrences")
				.send({
					target_taxons: target_taxons,
					latitud: 23.664650731631614,
					longitud: -101.98516868054867,
					sfecha: pair[0], 
					sfosil: pair[2],
					lininf: lim_inf,
					limsup: lim_sup,
					idtime: "1549052020331",
					grid_res: 16,
					region: 1
				})
				.expect("Content-type",/json/)
				.expect(200)
				.end(function(err, response){
					response.statusCode.should.equal(200)
					expect(response.body.data).all.have.property("species")
					expect(response.body.data).all.have.property("urlejemplar")
					expect(response.body.data).all.have.property("aniocolecta")
					expect(response.body.data).all.have.property("gridid")
					done();
				})

			});


		})

	});

});



describe("Prueba: Navegación de árbol taxonómico (variables bióticas)", function(){
	
	it("Resultado: Navegación de árbol taxonómico - OK", function(done){

		supertest(server).post("/niche/especie/getVariables")
		.send({
			  	field:"phylumdivisionvalido",
			  	parentfield:"especievalidabusqueda",
			  	parentitem:"Lynx rufus",
				footprint_region:1
			})
		.expect("Content-type",/json/)
		.expect(200)
		.end(function(err, response){
			response.statusCode.should.equal(200)
			expect(response.body.data).all.have.property("name")
			expect(response.body.data).all.have.property("spp")
			done();
		});
	});

});




/**
Variaciones posibles:
1. Solo bioticos OK 
2. Solo abioticos OK
3. Ambos OK
3. Celdas exlcuidas **
4. Todas la resoluciones OK
5. Todas las regiones OK
6. Filtro fechas **
7. Regsitros sin fecha **
8. Regsitros fosiles **
9. Carga de datos (otro verbo)
10. min de celdas **
11. apriori **
12. mapa probabildad **
**/


ba_var.forEach(pair => {
// [["true", "false"],["false", "true"],["true", "true"]].forEach(pair => {
// [["true", "false"]].forEach(pair => {

	describe("Prueba: Generación de análisis de nicho, variando bioticos, abioticos, resolution, region, apriori, probabildad", function(done){

		this.timeout(1000 * 60 * 10); // 3 minutos maximo
		
		var target_taxons = [{
			taxon_rank: "species",
			value: "Lynx rufus"
		}];

		var covariables = [];

		if(pair[0] === "true"){
			covariables.push({
				biotic: true,
				group_item: 1,
				merge_vars: [{
					level: "species",
					rank: "class",
					type: 0,
					value: "Mammalia"
				}],
				name: "mamiferos"
			})
		}

		if(pair[1] === "true"){
			covariables.push({
				biotic: false,
				group_item: 2,
				merge_vars: [{
					level: "bid",
					rank: "layer",
					type: 11,
					value: "bio095"
				}],
				name: "raster"
			})
		}

		// resoluciones de malla
		resoluciones.forEach(pair_res => {

			// Regiones activas (depende la version del sistema son las regiones activas)
			// 1. MX
			// 2. EU 
			// 3. MX y EU 
			// 4. COL
			// 5. Centro AME
			regiones.forEach(pair_reg => {

				// apriori y mapa de prob
				apr_prob.forEach(pair_apriprob => {
				// [["false", "false"], ["true", "false"],["false", "true"], ["true", "true"]].forEach(pair_apriprob => {

					describe("Prueba: bioticos | abioticos | resolution | region | apriori | probabildad => " + pair + "," + pair_res + "," + pair_reg + "," + pair_apriprob,function(){

						it("Resultado: bioticos | abioticos | resolution | region | apriori | probabildad => " + pair + "," + pair_res + "," + pair_reg + "," + pair_apriprob + " - OK", function(done){

							supertest(server).post("/niche/countsTaxonsGroup")
							.send({
								apriori: pair_apriprob[0],
								mapa_prob: pair_apriprob[1],
								min_cells: 5,
								region: 1,
								covariables: covariables,
								date: false,
								decil_selected: [10],
								excluded_cells: [],
								fosil: false,
								get_grid_species: false,
								grid_resolution: pair_res,
								target_taxons: target_taxons,
								target_points: [],
								time: 1610432318671,
								lim_inf:1500,
								lim_sup:2020,
								idtabla: "",
								iterations: 1,
								with_data_freq:false,
								with_data_freq_cell:false,
								with_data_score_cell:true,
								with_data_score_decil:true
							})
							.expect("Content-type",/json/)
							.expect(200)
							.end(function(err, response){
								response.statusCode.should.equal(200)
								expect(response.body.data).to.not.equal(null)
					            expect(response.body.data).all.have.property("group_name")
					            expect(response.body.data).all.have.property("reinovalido")
					            expect(response.body.data).all.have.property("phylumdivisionvalido")
					            expect(response.body.data).all.have.property("clasevalida")
					            expect(response.body.data).all.have.property("familiavalida")
					            expect(response.body.data).all.have.property("generovalido")
					            expect(response.body.data).all.have.property("familiavalida")
					            expect(response.body.data).all.have.property("especieepiteto")
					            expect(response.body.data).all.have.property("cells")
					            expect(response.body.data).all.have.property("ni")
					            expect(response.body.data).all.have.property("nij")
					            expect(response.body.data).all.have.property("nj")
					            expect(response.body.data).all.have.property("n")
					            expect(response.body.data).all.have.property("epsilon")
					            expect(response.body.data).all.have.property("score")

					            expect(response.body.data_score_cell).all.have.property("gridid")
					            expect(response.body.data_score_cell).all.have.property("tscore")

					            expect(response.body.validation_data).all.have.property("decil")
					            expect(response.body.validation_data).all.have.property("vp")
					            expect(response.body.validation_data).all.have.property("fn")
					            expect(response.body.validation_data).all.have.property("nulo")
					            expect(response.body.validation_data).all.have.property("recall")

					            expect(response.body.percentage_avg).all.have.property("decil")
					            expect(response.body.percentage_avg).all.have.property("epsilon")
					            expect(response.body.percentage_avg).all.have.property("score")
					            expect(response.body.percentage_avg).all.have.property("occ_perdecile")
					            expect(response.body.percentage_avg).all.have.property("occ")
					            expect(response.body.percentage_avg).all.have.property("species")

					            expect(response.body.decil_cells).all.have.property("cell")
					            expect(response.body.decil_cells).all.have.property("decile")

								done();
							})

						});

					}); 



				});

				
			}); // regiones

		}); // resolution

	}); // bioticos, abioticos


});



describe("Prueba: Generación de link de configuración de análisis en nicho", function(){
	
	it("Resultado: Generación de link de configuración de análisis en nicho - OK", function(done){

		supertest(server).post("/niche/especie/getToken")
		.send({
				tfilters:[{
					value: [{
						label: "Clase >> Mammalia",	
						level: 4,
						type: 0,
					}],
					type: 0,
					groupid: 1,
					title: "mamiferos"
				}],
				tipo: "nicho",
				sfilters:[{
					value: [{
						label: "Especie >> Lynx rufus",	
						level: 8,
						type: 0,
					}],
					type: 0,
					groupid: 1,
					title: "linces"
				}],
				val_process: false,
				idtabla: "no_table",
				mapa_prob: "false",
				fossil: "true",
				apriori: "false",
				sfecha: "true",
				min_occ: "5",
				grid_res: "16",
				footprint_region: "1",
				loadeddata:	"false"
             })
		.expect("Content-type",/json/)
		.expect(200)
		.end(function(err, response){
			response.statusCode.should.equal(200)
			expect(response.body.data).all.have.property("token")
			done();
		});
	});
	
});



describe("Prueba: Solicitud de link de configuración de análisis en nicho", function(){
	
	it("Resultado: Solicitud de link de configuración de análisis en nicho - OK", function(done){

		supertest(server).post("/niche/especie/getValuesFromToken")
		.send({
				"token":"3e1436bc11308b94ab5c54e3bf90ef59",
				"tipo":"nicho"
             })
		.expect("Content-type",/json/)
		.expect(200)
		.end(function(err, response){
			response.statusCode.should.equal(200)
			expect(response.body.data).all.have.property("parametros")
			done();
		});
	});
	
});