SELECT 
-- "type", layer, bid, label, icat, array_length(cells_64km::int[],1) 
-- sum(array_length(cells_$<resolution:raw>km::int[],1))  as spp 
count(*) as spp
FROM raster_bins
where $<where_clause_abio:raw>
group by "type"